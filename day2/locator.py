# test_web_elements
ADD_ELEMENT_BUTTON = '//button[text()="Add Element"]'
LAST_DELETE_BUTTON = '//button[text()="Delete"][last()]'
DELETE_BUTTON = 'button[class^="added"]'
LAST_DELETE_BUTTON_2 = '//button[contains(@class, "manually") and text()="Delete"][last()]'
LOREM_TEXT = '//td[text()="Apeirian9"]//preceding-sibling::td'
DOLOR_TEXT = '//td[text()="Apeirian9"]//following-sibling::td'

# test_wait
START_BUTTON = '//button[@id="startStopButton"]'
PROGRESS_BAR = '//div[@id="progressBar"]'

# test_form
PROGRAMMING_LANGUAGE_DROPDOWN = '//select[@id="dropdowm-menu-1"]'
FRUIT_DROPDOWN = '//select[@id="fruit-selects"]'
CHECKBOX = '//div[@id="checkboxes"]//input[@type="checkbox"]'
YELLOW_RADIO_BUTTON = '//form[@id="radio-buttons"]//input[@value="yellow"]'
