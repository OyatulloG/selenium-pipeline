# test_to_do_list
PRACTICE_MAGIC_BTN = '//li[contains(text(), "Practice magic")]'

# test_scrolling
LEFT_ENTRIES_BOX = '//div[@id="zone2"]'

# test_alerts
CLICK_ME_WITH_TIMER_BTN = '//button[@id="timerAlertButton"]'

# test_modal_dialogs
SMALL_MODAL_BTN = '//button[@id="showSmallModal"]'
MODAL_WINDOW = 'body > div.fade.modal.show'
MODAL_BODY_TEXTBOX = '//div[@class="modal-body"]'
SMALL_MODAL_TEXTBOX = '//div[contains(text(), "Small Modal")]'
CLOSE_SMALL_MODAL_BTN = '//button[@id="closeSmallModal"]'
