def remove_new_lines(text: str) -> str:
    return text.replace('\n', '')
