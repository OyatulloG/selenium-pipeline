# test_download
DOWNLOAD_FILES = '//div[@id="content"]//a[position() <= 3]'

# test_upload
FILE_UPLOAD_INPUT = '//input[@id="file-upload"]'
UPLOAD_BTN = '//input[@id="file-submit"]'
UPLOADED_FILES = '//div[@id="uploaded-files"]'

#test_cookies
USERNAME_INPUT = '//input[@name="username"]'
PASSWORD_INPUT = '//input[@name="password"]'
SUBMIT_BTN = '//button[@name="submit"]'
LOGGED_IN_TEXTBOX = '.form-signin-heading center'
