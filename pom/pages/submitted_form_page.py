from selenium.webdriver.common.by import By


class SubmittedFormPage:
    THANKS_FOR_SUBMIT_TEXTBOX = (By.XPATH, '//div[@id="example-modal-sizes-title-lg"]')
    FULL_NAME_TEXTBOX = (By.XPATH, '//td[contains(text(), "Student Name")]//following-sibling::td')
    GENDER_TEXTBOX = (By.XPATH, '//td[contains(text(), "Gender")]//following-sibling::td')
    MOBILE_TEXTBOX = (By.XPATH, '//td[contains(text(), "Mobile")]//following-sibling::td')
    HOBBIES_TEXTBOX = (By.XPATH, '//td[contains(text(), "Hobbies")]//following-sibling::td')
