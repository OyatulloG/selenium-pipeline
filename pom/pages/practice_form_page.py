from selenium.webdriver.common.by import By


class PracticeFormPage:
    FIRST_NAME_INPUT_BOX = (By.XPATH, '//input[@id="firstName"]')
    LAST_NAME_INPUT_BOX = (By.XPATH, '//input[@id="lastName"]')
    GENDER_MALE_RADIO_BUTTON = (By.XPATH, '//label[@for="gender-radio-1"]')
    MOBILE_NUMBER_INPUT_BOX = (By.XPATH, '//input[@id="userNumber"]')
    HOBBIES_READING_CHECKBOX = (By.XPATH, '//label[@for="hobbies-checkbox-2"]')
    SUBMIT_BUTTON = (By.XPATH, '//button[@id="submit"]')
