import allure

from pom.steps.common import CommonOps
from pom.pages.submitted_form_page import SubmittedFormPage


class SubmittedForm(CommonOps):
    sfp = SubmittedFormPage()

    @allure.step('Get "thanks for submit" message')
    def get_thanks_for_submit_message(self):
        return self.wait_for(self.sfp.THANKS_FOR_SUBMIT_TEXTBOX).text

    @allure.step('Get full_name')
    def get_full_name(self):
        return self.wait_for(self.sfp.FULL_NAME_TEXTBOX).text

    @allure.step('Get gender')
    def get_gender(self):
        return self.wait_for(self.sfp.GENDER_TEXTBOX).text

    @allure.step('Get mobile number')
    def get_mobile(self):
        return self.wait_for(self.sfp.MOBILE_TEXTBOX).text

    @allure.step('Get hobbies')
    def get_hobbies(self):
        return self.wait_for(self.sfp.HOBBIES_TEXTBOX).text