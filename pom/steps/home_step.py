import allure
from pom.steps.common import CommonOps
from pom.pages.home_page import HomePage


class Home(CommonOps):
    hp = HomePage()

    @allure.step('Navigate to Forms page')
    def navigate_to_forms_page(self):
        self.scroll_to(self.find(self.hp.FORMS_TEXT_BOX))
        self.wait_for(self.hp.FORMS_TEXT_BOX).click()
        return self
