import allure
from pom.steps.common import CommonOps
from pom.pages.practice_form_page import PracticeFormPage


class PracticeForm(CommonOps):
    pfp = PracticeFormPage()

    @allure.step('Enter first_name: {first_name}')
    def enter_first_name(self, first_name):
        self.wait_for(self.pfp.FIRST_NAME_INPUT_BOX).send_keys(first_name)
        return self

    @allure.step('Enter last_name: {last_name}')
    def enter_last_name(self, last_name):
        self.wait_for(self.pfp.LAST_NAME_INPUT_BOX).send_keys(last_name)
        return self

    @allure.step('Click gender: male radio button')
    def click_male_gender_radio_button(self):
        self.scroll_to(self.find(self.pfp.GENDER_MALE_RADIO_BUTTON))
        self.wait_for(self.pfp.GENDER_MALE_RADIO_BUTTON).click()
        return self

    @allure.step('Enter mobile_number: {mobile_number}')
    def enter_mobile_number(self, mobile_number):
        self.wait_for(self.pfp.MOBILE_NUMBER_INPUT_BOX).send_keys(mobile_number)
        return self

    @allure.step('Click hobby: reading checkbox')
    def click_reading_hobby_checkbox(self):
        self.wait_for(self.pfp.HOBBIES_READING_CHECKBOX).click()
        return self

    @allure.step('Submit form')
    def submit_form(self):
        self.wait_for(self.pfp.SUBMIT_BUTTON).submit()
        return self
