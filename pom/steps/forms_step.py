import allure
from pom.steps.common import CommonOps
from pom.pages.forms_page import FormsPage


class Forms(CommonOps):
    fp = FormsPage()

    @allure.step('Navigate to Practice Form page')
    def navigate_to_practice_form_page(self):
        self.wait_for(self.fp.PRACTICE_FORM_TEXTBOX_LINK).click()
        return self
