import allure
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class CommonOps:
    def __init__(self, driver):
        self.driver = driver
        self._wait = WebDriverWait(self.driver, 10)
        self._action = ActionChains(self.driver)

    @allure.step('Wait for {locator}')
    def wait_for(self, locator):
        return self._wait.until(ec.visibility_of_element_located(locator))

    @allure.step('Find {locator}')
    def find(self, locator):
        return self.driver.find_element(*locator)

    @allure.step('Scroll to {element}')
    def scroll_to(self, element):
        return self.driver.execute_script('arguments[0].scrollIntoView();', element)
