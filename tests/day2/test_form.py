from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
from day2 import locator


class TestWebForms:
    def test_dropdown_checkboxes_radiobuttons(self, driver):
        # Navigate to the http://webdriveruniversity.com/Dropdown-Checkboxes-RadioButtons/index.html
        driver.get('http://webdriveruniversity.com/Dropdown-Checkboxes-RadioButtons/index.html')

        # Choose programming language from dropdown and ensure that it was selected
        programming_language_dropdown = Select(driver.find_element(By.XPATH, locator.PROGRAMMING_LANGUAGE_DROPDOWN))
        programming_language_dropdown.select_by_visible_text('Python')
        assert programming_language_dropdown.first_selected_option.text == 'Python', \
            'Expected programming language is not selected.'

        # In 'Selected & Disabled' section check that `Orange` option in dropdown is disabled
        fruit_dropdown = Select(driver.find_element(By.XPATH, locator.FRUIT_DROPDOWN))
        orange_option_of_fruit_dropdown = None
        for option in fruit_dropdown.options:
            if option.text == 'Orange':
                orange_option_of_fruit_dropdown = option
        assert not orange_option_of_fruit_dropdown.is_enabled(), 'Orange option of Fruit Dropdown is enabled.'

        # **(Optional)** find and Click on all unselected checkboxes
        checkboxes = driver.find_elements(By.XPATH, locator.CHECKBOX)
        for checkbox in checkboxes:
            if not checkbox.is_selected():
                checkbox.click()

        for checkbox in checkboxes:
            assert checkbox.is_selected() is True, 'CheckBox is not selected.'

        # **(Optional)** Click on `Yellow` radio button
        yellow_radio_button = driver.find_element(By.XPATH, locator.YELLOW_RADIO_BUTTON)

        assert yellow_radio_button.is_selected() is False, 'Yellow radio button is selected.'
        yellow_radio_button.click()
        assert yellow_radio_button.is_selected() is True, 'Yellow radio button is not selected.'
