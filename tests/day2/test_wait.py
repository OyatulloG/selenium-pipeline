from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from day2 import locator


class TestWait:
    def test_progress_bar(self, driver):
        # Navigate to the https://demoqa.com/progress-bar
        driver.get('https://demoqa.com/progress-bar')

        # Click to `Start` button
        start_button = driver.find_element(By.XPATH, locator.START_BUTTON)
        start_button.click()

        # Use explicit wait until progress bar reach `100 %` and print `100%`
        progress_bar_reached_100_percent = WebDriverWait(driver, 15) \
            .until(ec.text_to_be_present_in_element((By.XPATH, locator.PROGRESS_BAR), '100%'))
        assert progress_bar_reached_100_percent is True, 'Progress bar has not reached 100%'
        print(f'>>>>> Progress Bar: 100%')
