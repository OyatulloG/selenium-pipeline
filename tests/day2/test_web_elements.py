from day2 import locator
from selenium.webdriver.common.by import By


class TestWebElements:
    def test_add_remove_elements(self, driver):
        # Navigate to the http://the-internet.herokuapp.com/add_remove_elements/
        driver.get('http://the-internet.herokuapp.com/add_remove_elements/')

        # Click to `Add element` three times
        add_element_button = driver.find_element(By.XPATH, locator.ADD_ELEMENT_BUTTON)
        for i in range(3):
            add_element_button.click()

        # Print out the last `Delete` button element with `find_element()`
        last_delete_button = driver.find_element(By.XPATH, locator.LAST_DELETE_BUTTON)
        print(f'>>>>> 1) Last Delete Button: {last_delete_button}')

        # Print out the last `Delete` button with `find_elements()`.
        # Use `cssSelector` as locator of element with class name, that starts with `added` word
        delete_buttons = driver.find_elements(By.CSS_SELECTOR, locator.DELETE_BUTTON)
        print(f'>>>>> 2) Last Delete Button: {delete_buttons[2]}')

        # Print out the last `Delete` button element with find_element().
        # Use relative `XPath` with tag, that can accept any char with class name
        #   that contains `manually`' and text() `Delete`
        last_delete_button_2 = driver.find_element(By.XPATH, locator.LAST_DELETE_BUTTON_2)
        print(f'>>>>> 3) Last Delete Button: {last_delete_button_2}')

        assert last_delete_button == delete_buttons[2] and delete_buttons[2] == last_delete_button_2, \
            'Three last delete buttons are not the same.'

    def test_challenging_dom(self, driver):
        # Navigate to the http://the-internet.herokuapp.com/challenging_dom
        driver.get('http://the-internet.herokuapp.com/challenging_dom')

        # Using relative `XPath`, Print out the Lorem value of element, that has `Apeirian9` as Ipsum value
        lorem_text = driver.find_element(By.XPATH, locator.LOREM_TEXT)
        print(f'>>>>> Lorem value preceding "Apeirian9" Ipsum: {lorem_text.text}')
        assert lorem_text.text == 'Iuvaret9', 'Lorem value is not equal to the expected one.'

        # Using relative `XPath`, Print out the next element of element with Ipsum value `Apeirian9`
        dolor_text = driver.find_element(By.XPATH, locator.DOLOR_TEXT)
        print(f'>>>>> Dolor value following "Apeirian9" Ipsum: {dolor_text.text}')
        assert dolor_text.text == 'Adipisci9', 'Dolor value is not equal to the expected one.'
