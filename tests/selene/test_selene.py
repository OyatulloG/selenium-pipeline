from selene import by, have


class TestSelene:
    def test_checkboxes(self, browser):
        # Navigate to the http://the-internet.herokuapp.com/checkboxes
        browser.open('http://the-internet.herokuapp.com/checkboxes')

        # Set first checkbox selected
        browser.element(by.xpath('//input[@type="checkbox"][1]')).click()

        # Validate that both checkboxes has type 'checkbox'
        checkboxes = browser.all(by.xpath('//input[@type="checkbox"]'))
        for checkbox in checkboxes:
            checkbox.should(have.attribute('type').value('checkbox'))

    def test_text_box(self, browser):
        full_name = 'Sherlock Holmes'
        email = 'sherlock_holmes@gmail.com'
        current_address = "Baker's Street 21A"
        permanent_address = "England, London, Baker's Str 21A"

        # Navigate to the https://demoqa.com/text-box
        browser.open('https://demoqa.com/text-box')

        # Fill fullname, valid email,current and permanent addresses using different selectors
        browser.element(by.id('userName')).send_keys(full_name)
        browser.element(by.css('#userEmail')).send_keys(email)
        browser.element(by.xpath('//textarea[@id="currentAddress"]')).send_keys(current_address)
        browser.element(by.id('permanentAddress')).send_keys(permanent_address)
        browser.element('//button[@id="submit"]').click()

        # Validate the result with Collection Assertion
        expected_info = ['Name:' + full_name,
                         'Email:' + email,
                         'Current Address :' + current_address,
                         'Permananet Address :' + permanent_address]
        browser.all('.mb-1').should(have.exact_texts(*expected_info))
