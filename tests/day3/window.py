from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from day3 import locator


class TestNewWindow:
    def test_browser_window(self, driver):
        # Navigate to the https://demoqa.com/browser-windows
        driver.get('https://demoqa.com/browser-windows')

        # Click on `New Tab`
        new_tab_button = driver.find_element(By.XPATH, locator.NEW_TAB_BUTTON)
        new_tab_button.click()

        # switch to that tab
        original_window = driver.current_window_handle
        WebDriverWait(driver, 5).until(EC.number_of_windows_to_be(2))

        for window_handle in driver.window_handles:
            if window_handle != original_window:
                driver.switch_to.window(window_handle)
                break

        # Check that new page has message `This is a sample page`
        text_exists = driver.page_source.__contains__('This is a sample page')
        assert text_exists is True, 'The page does not have an expected text.'

        # Return to previous tab
        driver.switch_to.window(original_window)
