from selenium.webdriver.common.by import By
from day3 import locator


class TestWebTables:
    def test_webtable(self, driver):
        # Navigate to the http://techcanvass.com/Examples/webtable.html
        driver.get('http://techcanvass.com/Examples/webtable.html')

        # Print honda's price
        honda_price = driver.find_element(By.XPATH, locator.HONDA_PRICE)
        print(f">>>>> Honda's price = {honda_price.text}")
        assert honda_price.text == '6,00,000', "Honda's price is not equal to the expected one."
