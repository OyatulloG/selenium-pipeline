from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from day3 import locator
from day3 import util


class TestSwitchToIframe:
    def test_iframe(self, driver):
        # Navigate to the http://the-internet.herokuapp.com/iframe
        driver.get('http://the-internet.herokuapp.com/iframe')

        # Write text 'Here Goes' in text field
        driver.switch_to.frame(locator.IFRAME)
        input_text_field = driver.find_element(By.XPATH, locator.INPUT_TEXT_FIELD)
        input_text_field.clear()

        input_text_field.send_keys('Here Goes')
        input_text_field = driver.find_element(By.XPATH, locator.INPUT_TEXT_FIELD)
        assert input_text_field.text == 'Here Goes', 'Expected Text is not written to the text field.'

        # Click on 'Align center' icon
        driver.switch_to.default_content()
        align_center_button = driver.find_element(By.XPATH, locator.ALIGN_CENTER_BUTTON)
        align_center_button.click()

        driver.switch_to.frame(locator.IFRAME)
        text_align_style = util.get_text_align(input_text_field.get_attribute('style'))
        assert text_align_style == 'center', '"Align center" style is not applied for the text field.'

    def test_alerts(self, driver):
        # Navigate to the https://demoqa.com/alerts
        driver.get('https://demoqa.com/alerts')

        # Click on the first 'Click me' button and try to accept unexpected alert
        click_me_alert_button = driver.find_element(By.XPATH, locator.CLICK_ME_ALERT_BUTTON)
        click_me_alert_button.click()

        alert = WebDriverWait(driver, 5).until(EC.alert_is_present())
        alert_text = alert.text
        assert alert_text == 'You clicked a button', 'Expected alert message is not displayed.'
        alert.accept()
