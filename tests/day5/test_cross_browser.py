from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from day4 import locator


class TestCrossBrowser:
    def test_to_do_list(self, driver_config):
        # Navigate to the http://webdriveruniversity.com/To-Do-List/index.html
        driver_config.get('http://webdriveruniversity.com/To-Do-List/index.html')

        # Hover on "Practice Magic" todo item using ActionChains Class, moveToElement
        actions = ActionChains(driver_config)
        practice_magic_btn = driver_config.find_element(By.XPATH, locator.PRACTICE_MAGIC_BTN)
        actions.move_to_element(practice_magic_btn).perform()

        practice_magic_btn_hovered = driver_config.execute_script("return arguments[0].matches(':hover')",
                                                                  practice_magic_btn)
        assert practice_magic_btn_hovered is True, 'Practice Magic Button is not hovered'

        # Delete item using JS Executor
        driver_config.execute_script('arguments[0].remove()', practice_magic_btn)

        practice_magic_btn_not_exists = WebDriverWait(driver_config, 5) \
            .until(EC.invisibility_of_element(practice_magic_btn))
        assert practice_magic_btn_not_exists is True, 'Practice Magic button is not deleted from the page.'
