from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from day4 import locator
from day5 import utils


class TestChromeOptions:
    def test_to_do_list(self, headless_driver):
        # Navigate to the http://webdriveruniversity.com/To-Do-List/index.html
        headless_driver.get('http://webdriveruniversity.com/To-Do-List/index.html')

        # Hover on "Practice Magic" todo item using ActionChains Class, moveToElement
        actions = ActionChains(headless_driver)
        practice_magic_btn = headless_driver.find_element(By.XPATH, locator.PRACTICE_MAGIC_BTN)
        actions.move_to_element(practice_magic_btn).perform()

        practice_magic_btn_hovered = headless_driver.execute_script("return arguments[0].matches(':hover')", practice_magic_btn)
        assert practice_magic_btn_hovered is True, 'Practice Magic Button is not hovered'

        # Delete item using JS Executor
        headless_driver.execute_script('arguments[0].remove()', practice_magic_btn)

        practice_magic_btn_not_exists = WebDriverWait(headless_driver, 5) \
            .until(EC.invisibility_of_element(practice_magic_btn))
        assert practice_magic_btn_not_exists is True, 'Practice Magic button is not deleted from the page.'

    def test_expired_bad_ssl_com(self, certificate_ignoring_driver):
        # navigate to https://expired.badssl.com/ and handle ssl certificate issue using chrome options
        certificate_ignoring_driver.get('https://expired.badssl.com/')

        # validate that h1 tag has `expired.badssl.com` message
        h1_textbox = certificate_ignoring_driver.find_element(By.TAG_NAME, 'h1')
        h1_textbox_text = utils.remove_new_lines(h1_textbox.text)
        assert h1_textbox_text.__contains__('expired.badssl.com') is True, \
            'Element with tag h1 does not contain the expected text.'
