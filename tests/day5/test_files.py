import time
import os
from pathlib import Path
from selenium.webdriver.common.by import By

from day5 import locator


class TestFileUpload:
    def test_download(self, driver):
        # open https://the-internet.herokuapp.com/download
        driver.get('https://the-internet.herokuapp.com/download')

        # find first 3 files and download them
        files = driver.find_elements(By.XPATH, locator.DOWNLOAD_FILES)
        for file in files:
            file.click()

        time.sleep(5)

        # verify downloaded files
        default_downloads_dir = os.path.join(Path.cwd(), 'day5', 'download')

        file_count = 0
        for path in os.listdir(default_downloads_dir):
            # check if current path is a file
            if os.path.isfile(os.path.join(default_downloads_dir, path)):
                file_count += 1

        assert file_count == 3, 'Expected files are not downloaded.'

    def test_upload(self, driver):
        # navigate to https://the-internet.herokuapp.com/upload
        driver.get('https://the-internet.herokuapp.com/upload')

        # upload some file
        file_upload_input = driver.find_element(By.XPATH, locator.FILE_UPLOAD_INPUT)
        file_upload_input.send_keys(f'{Path.cwd()}/README.md')

        # click Upload
        upload_btn = driver.find_element(By.XPATH, locator.UPLOAD_BTN)
        upload_btn.click()

        # Validate that File Uploaded!
        uploaded_files = driver.find_element(By.XPATH, locator.UPLOADED_FILES)
        assert uploaded_files.text == 'README.md', 'Expected file is not uploaded.'
