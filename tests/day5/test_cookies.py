from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from day5 import locator


class TestBrowserCookies:
    def test_cookies(self, driver):
        # create one login case at http://demo.guru99.com/test/cookie/selenium_aut.php
        driver.get('http://demo.guru99.com/test/cookie/selenium_aut.php')

        username_input = driver.find_element(By.XPATH, locator.USERNAME_INPUT)
        username_input.send_keys('OyatulloG')
        password_input = driver.find_element(By.XPATH, locator.PASSWORD_INPUT)
        password_input.send_keys('strong_password_:)')
        submit_btn = driver.find_element(By.XPATH, locator.SUBMIT_BTN)
        submit_btn.submit()

        # get all cookies
        cookies = driver.get_cookies()
        for cookie in cookies:
            print(f"{cookie['name']}, ")

        # using the new driver, open a new session http://demo.guru99.com/test/cookie/selenium_aut.php
        chrome_service = Service(executable_path=ChromeDriverManager().install())
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--window-size=1920x1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        new_driver = webdriver.Chrome(options=chrome_options, service=chrome_service)
        new_driver.get('http://demo.guru99.com/test/cookie/selenium_aut.php')

        # delete all cookies
        new_cookies = new_driver.get_cookies()
        for ck in new_cookies:
            print(f"{ck['name']} {ck['value']}")
            new_driver.delete_cookie(ck['name'])

        after_deleting_cookies = new_driver.get_cookies()
        for ck in after_deleting_cookies:
            print(f"Not deleted cookies {ck['name']}")

        # add cookies from previous login session
        for ck in cookies:
            new_driver.add_cookie(ck)

        # refresh the page
        new_driver.refresh()

        # Confirm that you are logged in
        logged_in_textbox = new_driver.find_element(By.CSS_SELECTOR, locator.LOGGED_IN_TEXTBOX)
        assert logged_in_textbox.text == "You are logged In", 'Log in is not performed.'
