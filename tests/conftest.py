import allure
from datetime import datetime
import os
import pytest
from selenium import webdriver
from pathlib import Path
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.edge.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selene import Browser, Config
from selenium.webdriver import Chrome


@pytest.fixture(scope='session')
def driver():
    chrome_service = Service(executable_path=ChromeDriverManager().install())
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    default_downloads_dir = os.path.join(Path.cwd(), 'day5', 'download')
    prefs = {
        'download.default_directory': default_downloads_dir
    }
    chrome_options.add_experimental_option('prefs', prefs)
    driver = webdriver.Chrome(options=chrome_options, service=chrome_service)
    driver.implicitly_wait(time_to_wait=5)
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def browser():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    browser = Browser(
        Config(
            driver=Chrome(options=chrome_options),
            timeout=2,
        )
    )
    yield browser
    browser.quit()


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # print(item.fixturenames)
    outcome = yield
    rep = outcome.get_result()

    if rep.when == 'call' and rep.failed:
        print(f"{datetime.now()} - Test {item.nodeid} failed.")

        os.makedirs('screenshots', exist_ok=True)

        # driver = item.funcargs['driver']
        #
        # take_screenshot(driver, item.nodeid)

        try:
            if 'driver' in item.fixturenames:
                browser_driver = item.funcargs['driver']
                take_screenshot(browser_driver, item.nodeid)
            if 'browser' in item.fixturenames:
                browser_driver = item.funcargs['browser']
                take_screenshot(browser_driver.driver, item.nodeid)
            # else:
            #     take_screenshot(web_browser.driver, item.nodeid)
        except Exception as e:
            print(f'Exception while screen-shot creation: {e}')


def take_screenshot(driver, nodeid):
    now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    filename = f"{nodeid}_{now}.png".replace("/", "_").replace("::", "__")
    driver.save_screenshot(f"screenshots/{filename}")
    allure.attach(driver.get_screenshot_as_png(), name=filename, attachment_type=allure.attachment_type.PNG)


@pytest.fixture(scope='session')
def headless_driver():
    chrome_service = Service(executible_path=ChromeDriverManager().install())
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--incognito')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--window-size=1920x1080')
    driver = webdriver.Chrome(options=chrome_options, service=chrome_service)
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def certificate_ignoring_driver():
    chrome_service = Service(executible_path=ChromeDriverManager().install())
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--ignore-certificate-errors')
    driver = webdriver.Chrome(options=chrome_options, service=chrome_service)
    yield driver
    driver.quit()


@pytest.fixture(params=['chrome', 'firefox'], scope='session')
def driver_config(request):
    if request.param == 'chrome':
        chrome_service = Service(executable_path=ChromeDriverManager().install())
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--window-size=1920x1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(options=chrome_options, service=chrome_service)
    elif request.param == 'firefox':
        firefox_service = Service(executable_path=GeckoDriverManager().install())
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.add_argument('--window-size=1920x1080')
        firefox_options.add_argument('--headless')
        driver = webdriver.Firefox(options=firefox_options, service=firefox_service)
    elif request.param == 'edge':
        edge_service = Service(executable_path=EdgeChromiumDriverManager().install())
        edge_options = webdriver.EdgeOptions()
        edge_options.add_argument('--window-size=1920x1080')
        edge_options.add_argument('--headless')
        driver = webdriver.Firefox(options=edge_options, service=edge_service)
    else:
        raise ValueError('Unsupported browser')

    driver.implicitly_wait(5)
    yield driver
    driver.quit()
