import allure
import pytest
from pom.steps.home_step import Home
from pom.steps.forms_step import Forms
from pom.steps.practice_form_step import PracticeForm
from pom.steps.submitted_form_step import SubmittedForm
from pom.data.form_data import FormData


@allure.epic('POM')
class TestDemoqa:
    test_data = [
        (FormData.FIRST_NAME, FormData.LAST_NAME, FormData.MOBILE_NUMBER),
        ('Sherlock', 'Holmes', '1234567899')
    ]

    @allure.feature('POM features')
    @pytest.mark.parametrize('firstname, lastname, mobile', test_data)
    @allure.title('Test POM: Form info - Firstname: {firstname}, Lastname: {lastname}, Mobile: {mobile}')
    @allure.description('The test navigates to Practice Form, fills the form with sample data and verifies that given '
                        'data matches with the expected.')
    @allure.severity(allure.severity_level.CRITICAL)
    def test_pom(self, driver, firstname, lastname, mobile):
        home = Home(driver)
        forms = Forms(driver)
        practice_form = PracticeForm(driver)
        submitted_form = SubmittedForm(driver)

        # Navigate to the https://demoqa.com/
        driver.get('https://demoqa.com/')

        # Click on `Forms`
        home.navigate_to_forms_page()

        # Click on `Practice Form`
        forms.navigate_to_practice_form_page()

        # Fill First Name, Last Name , Gender and mobile number, Hobbies
        practice_form.enter_first_name(firstname)
        practice_form.enter_last_name(lastname)
        practice_form.click_male_gender_radio_button()
        practice_form.enter_mobile_number(mobile)
        practice_form.click_reading_hobby_checkbox()
        practice_form.submit_form()

        # Check that `Thanks for submitting the form` text is visible
        with allure.step('Check that displayed "Thanks for submit" message matches with the expected one.'):
            assert submitted_form.get_thanks_for_submit_message() == 'Thanks for submitting the form', \
                'The form is not submitted and expected thanks message is not displayed.'

        # Check that student info is correct
        full_name = firstname + " " + lastname
        with allure.step('Check that displayed Fullname matches with the expected one.'):
            assert submitted_form.get_full_name() == full_name, 'Expected fullname is not displayed.'
        with allure.step('Check that displayed Gender matches with the expected one.'):
            assert submitted_form.get_gender() == FormData.GENDER, 'Expected gender is not displayed.'
        with allure.step('Check that displayed Mobile number matches with the expected one.'):
            assert submitted_form.get_mobile() == mobile, 'Expected mobile number is not displayed.'
        with allure.step('Check that displayed Hobbies match with the expected ones.'):
            assert submitted_form.get_hobbies() == FormData.HOBBIES, 'Expected hobby is not displayed.'

    @allure.story('Fluent API')
    @allure.title('POM features with Fluent API')
    def test_fluent(self, driver):
        home = Home(driver)
        forms = Forms(driver)
        practice_form = PracticeForm(driver)
        submitted_form = SubmittedForm(driver)

        # Navigate to the https://demoqa.com/
        driver.get('https://demoqa.com/')

        # Click on `Forms`
        home.navigate_to_forms_page()

        # Click on `Practice Form`
        forms.navigate_to_practice_form_page()

        # Fill First Name, Last Name , Gender and mobile number, Hobbies
        (practice_form.enter_first_name(FormData.FIRST_NAME)
         .enter_last_name(FormData.LAST_NAME)
         .click_male_gender_radio_button()
         .enter_mobile_number(FormData.MOBILE_NUMBER)
         .click_reading_hobby_checkbox()
         .submit_form())

        # Check that `Thanks for submitting the form` text is visible
        with allure.step('Check that displayed "Thanks for submit" message matches with the expected one.'):
            assert submitted_form.get_thanks_for_submit_message() == 'Thanks for submitting the form', \
                'The form is not submitted and expected thanks message is not displayed.'

        # Check that student info is correct
        full_name = FormData.FIRST_NAME + " " + FormData.LAST_NAME
        with allure.step('Check that displayed Fullname matches with the expected one.'):
            assert submitted_form.get_full_name() == full_name, 'Expected fullname is not displayed.'
        with allure.step('Check that displayed Gender matches with the expected one.'):
            assert submitted_form.get_gender() == FormData.GENDER, 'Expected gender is not displayed.'
        with allure.step('Check that displayed Mobile number matches with the expected one.'):
            assert submitted_form.get_mobile() == FormData.MOBILE_NUMBER, 'Expected mobile number is not displayed.'
        with allure.step('Check that displayed Hobbies match with the expected ones.'):
            assert submitted_form.get_hobbies() == FormData.HOBBIES, 'Expected hobby is not displayed.'
