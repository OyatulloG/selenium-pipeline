from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from day1 import locator
from day1 import constant
import time


def test_first_laptop_in_demoblaze():
    chrome_service = Service(executable_path=ChromeDriverManager().install())
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=chrome_options, service=chrome_service)

    driver.implicitly_wait(10)
    # driver.maximize_window()

    # Navigate to the https://www.demoblaze.com/index.html
    driver.get('https://www.demoblaze.com/index.html')

    # Click Laptops Category
    laptops_category_button = driver.find_element(By.XPATH, locator.LAPTOPS_CATEGORY_BUTTON)
    laptops_category_button.click()

    time.sleep(2)  # wait for the Laptops page to be loaded

    # Find first element product title and price
    # Check results
    first_laptop_title = driver.find_element(By.XPATH, locator.FIRST_LAPTOP_TITLE_TEXT)
    assert first_laptop_title.text == constant.FIRST_LAPTOP_TITLE, \
        'First Laptop Title does not match with the expected title.'

    first_laptop_price = driver.find_element(By.XPATH, locator.FIRST_LAPTOP_PRICE_TEXT)
    assert first_laptop_price.text == constant.FIRST_LAPTOP_PRICE, \
        'First Laptop Price does not match with the expected price.'
