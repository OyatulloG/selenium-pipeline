from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from day1 import locator


def test_enable_button_in_dynamic_controls():
    chrome_service = Service(executable_path=ChromeDriverManager().install())
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=chrome_options, service=chrome_service)

    driver.implicitly_wait(10)
    # driver.maximize_window()

    # Navigate to the http://the-internet.herokuapp.com/dynamic_controls
    driver.get('http://the-internet.herokuapp.com/dynamic_controls')

    # Click on Enable button
    enable_button = driver.find_element(By.XPATH, locator.ENABLE_BUTTON)
    enable_button.click()

    # Check that input field became enabled and text 'It's enabled!' is displayed
    input_field_message = WebDriverWait(driver, 10) \
        .until(ec.visibility_of_element_located((By.XPATH, locator.INPUT_FIELD_MESSAGE)))
    input_field = driver.find_element(By.XPATH, locator.INPUT_FIELD)
    assert input_field.is_enabled() is True, 'Input Field is disabled.'
    assert input_field_message.text == "It's enabled!", 'Enabled text is not displayed.'

    # Check that button text has changed from Enable to Disable
    assert enable_button.text == 'Disable', 'Button text is not "Disable".'

    # Write `your_name` in input field and clear it
    input_field.send_keys('Oyatullo')
    input_field.clear()
