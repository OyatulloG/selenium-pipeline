from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from day1 import locator


def test_y_coordinate_for_columns_in_drag_and_drop():
    chrome_service = Service(executable_path=ChromeDriverManager().install())
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=chrome_options, service=chrome_service)

    driver.implicitly_wait(10)
    # driver.maximize_window()

    # Navigate to the http://the-internet.herokuapp.com/drag_and_drop
    driver.get('http://the-internet.herokuapp.com/drag_and_drop')

    # Check that Y coordinate of column A and column B are the same
    column_a = driver.find_element(By.XPATH, locator.COLUMN_A)
    column_b = driver.find_element(By.XPATH, locator.COLUMN_B)
    assert column_a.location['y'] == column_b.location['y'], \
        'Column A and Column B have different Y coordinates'
