from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from day4 import locator


class TestExceptions:
    def test_alerts(self, driver):
        # Navigate to the https://demoqa.com/alerts
        driver.get('https://demoqa.com/alerts')

        # Click on `Click me` button with id `timerAlertButton`
        click_me_btn = driver.find_element(By.XPATH, locator.CLICK_ME_WITH_TIMER_BTN)
        click_me_btn.click()

        # Try to invoke `TimeOutException` exception
        try:
            WebDriverWait(driver, 1).until(EC.alert_is_present())
            driver.switch_to.alert.accept()
        except TimeoutException:
            print(f'>>>>> TimeoutException is thrown.')

        # Try to avoid `NoAlertPresentException` and Handle possible exception
        try:
            driver.switch_to.alert.accept()
        except NoAlertPresentException:
            print(f'>>>>> NoAlertPresentException is thrown.')

        # **(Optional)** Try to invoke `StaleElementReferenceException`
        try:
            driver.refresh()
            click_me_btn.click()
        except StaleElementReferenceException:
            print(f'>>>>> StaleElementReferenceException is thrown.')
