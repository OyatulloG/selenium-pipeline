from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from day4 import locator


class TestJSexecutor:
    def test_to_do_list(self, driver):
        # Navigate to the http://webdriveruniversity.com/To-Do-List/index.html
        driver.get('http://webdriveruniversity.com/To-Do-List/index.html')

        # Hover on "Practice Magic" todo item using ActionChains Class, moveToElement
        actions = ActionChains(driver)
        practice_magic_btn = driver.find_element(By.XPATH, locator.PRACTICE_MAGIC_BTN)
        actions.move_to_element(practice_magic_btn).perform()

        practice_magic_btn_hovered = driver.execute_script("return arguments[0].matches(':hover')", practice_magic_btn)
        assert practice_magic_btn_hovered is True, 'Practice Magic Button is not hovered'

        # Delete item using JS Executor
        driver.execute_script('arguments[0].remove()', practice_magic_btn)

        practice_magic_btn_not_exists = WebDriverWait(driver, 5) \
            .until(EC.invisibility_of_element(practice_magic_btn))
        assert practice_magic_btn_not_exists is True, 'Practice Magic button is not deleted from the page.'

    def test_scrolling(self, driver):
        # Navigate to the http://webdriveruniversity.com/Scrolling/index.html
        driver.get('http://webdriveruniversity.com/Scrolling/index.html')

        # Scroll to the left box with 'Entries' text
        actions = ActionChains(driver)
        left_entries_box = driver.find_element(By.XPATH, locator.LEFT_ENTRIES_BOX)
        actions.move_to_element(left_entries_box).perform()

        # Validate text with JS executor
        left_entries_box_text = driver.execute_script('return arguments[0].innerText', left_entries_box)
        assert left_entries_box_text == '1 Entries', 'The displayed text is not equal to the expected one.'
