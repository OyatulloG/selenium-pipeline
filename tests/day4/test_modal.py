from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from day4 import locator


class TestModalDialogs:
    def test_modal_dialogs(self, driver):
        # Navigate to the https://demoqa.com/modal-dialogs
        driver.get('https://demoqa.com/modal-dialogs')

        # Click on `Small modal` using JS
        small_modal_btn = driver.find_element(By.XPATH, locator.SMALL_MODAL_BTN)
        driver.execute_script('arguments[0].click()', small_modal_btn)

        # Wait until Modal Window appears
        # (has css style display `block` and class has `show`) with css selector 'body > div.fade.modal.show'
        WebDriverWait(driver, 2) \
            .until(EC.visibility_of_element_located((By.CSS_SELECTOR, locator.MODAL_WINDOW)))

        # Check that "Small Modal" present
        small_modal_textbox = driver.find_element(By.XPATH, locator.SMALL_MODAL_TEXTBOX)
        small_modal_textbox_exists = small_modal_textbox.is_displayed()
        assert small_modal_textbox_exists is True, 'Small Modal is not displayed on the page.'

        # Check that message contains "This is a small modal"
        modal_body_textbox = driver.find_element(By.XPATH, locator.MODAL_BODY_TEXTBOX)
        assert modal_body_textbox.text.__contains__('This is a small modal') is True, \
            'The expected message is not displayed.'

        # **(Optional)** Check that `Close` button is enabled and click on it
        close_small_modal_btn = driver.find_element(By.XPATH, locator.CLOSE_SMALL_MODAL_BTN)
        assert close_small_modal_btn.is_enabled() is True, 'Close button is not enabled.'
        close_small_modal_btn.click()

        # **(Optional)** Make sure the modal dialog is closed
        modal_dialog_closed = WebDriverWait(driver, 2) \
            .until(EC.invisibility_of_element_located((By.CSS_SELECTOR, locator.MODAL_WINDOW)))
        assert modal_dialog_closed is True, 'Modal Dialog is not closed.'
