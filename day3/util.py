def get_text_align(style: str) -> str:
    return style[:-1].split()[1]