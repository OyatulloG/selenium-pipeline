# test_iframe
IFRAME = 'mce_0_ifr'
INPUT_TEXT_FIELD = '//body[@id="tinymce"]//p'
ALIGN_CENTER_BUTTON = '//button[@title="Align center"]'

# test_alerts
CLICK_ME_ALERT_BUTTON = '//button[@id="alertButton"]'

# test_webtable
HONDA_PRICE = '//td[text()="Honda"]//following-sibling::td[2]'

# test_browser_window
NEW_TAB_BUTTON = '//button[@id="tabButton"]'
